# JS Programming Task

Javascript is Loncel's main programming language in the cloud space. It is quite important
that you feel comfortable with it. Hence this test.

The result of this task should be a working application, that can be installed and executed from the terminal.

*Note: We don't consider this as a big task. Take as much time as you need. Important is the result.*

## Prerequisites

- Please note that this will require some basic [JavaScript](http://www.codecademy.com/tracks/javascript) and [ExpressJS](http://expressjs.com/) knowledge, depending on how you want to approach the task.

- There is no further requirement, use the JS frameworks of your choice

## Task

1. Fork this repository (if you don't know how to do that, Google is your friend)
2. Create a *source* folder to contain your code.
3. In the *source* directory, please create an ExpressJS app that accomplishes the following:
	- Connect to the [Github API](http://developer.github.com/)
	- Find the [nodejs](https://github.com/nodejs/node) repository
	- Find the most recent commits (choose at least 25 or more of the commits)
	- Create a route that displays the recent commits ordered by author.
	- If the commit hash ends in a number, color that row to red (#DC143C).

### Tests

Create the following unit test with the testing framework of your choice:

  1.  Verify that rows ending in a number are colored red.  

## Once Complete
1. Commit and Push your code to your new repository
2. Send us a pull request, we will review your code and get back to you
